#include<iostream>
#include"calculator.h"
#include"inputHandler.h" 
using namespace std;

// Function to make mathematical computations based on what statement is givin
float calculate(float a, float b, string statement)
{
    if(statement == "plus")
    {
        return a + b;
    }
    else if(statement == "minus")
    {    
        return a - b;
    }
    else if (statement == "times")
    {
        return a * b;
    }
    else if(statement == "over")
    {
        // Check to make sure we don't divide by 0
        if (b == 0)
        {
            cout << "Can't divided by 0!" << endl;
            take_inputs();
            return 0;
        }
        else
        {
            return a / b;
        }

    }
    // If a statement was not recognized we promted the user to reenter inputs
    else
    {
        cout << statement << "is not correct" << endl;
        take_inputs();
        return 0;
    }
}