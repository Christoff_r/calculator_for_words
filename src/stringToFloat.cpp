#include<iostream>
#include"stringToFloat.h"
#include"inputHandler.h"
using namespace std;

// Map of written numbers and the numeric value
map<string, float> string_to_int_map = {{"zero", 0}, {"one", 1}, {"two", 2}
                                    , {"three", 3}, {"four", 4}, {"five", 5}
                                    , {"six", 6}, {"seven", 7}, {"eight", 8}, {"nine", 9}};


// Funtion to convet string to float
float string_to_float(string s)
{
    // Loop over the map pairs
    for (auto entry : string_to_int_map)
    {
        // Save the key and value
        string key = entry.first;
        int value  = entry.second;

        // If the argument macthes a key we return its value
        if(key == s)
        {
            return value;
        }
    }

    // If the argument was not found we promt the user to reenter input
    cout << s << " is not a written number." << endl;
    take_inputs();
    return 0;
}