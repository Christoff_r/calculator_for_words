#include<iostream>
#include"inputHandler.h"
#include"stringToFloat.h"
#include"calculator.h"
using namespace std;

string first_string;
string sencond_string;
string statement;

float first_float;
float second_float;
float result;

void take_inputs()
{
    // Promte user to write two and a mathematical statement
    cout << "Write 2 numbes betweem 0-9 sperated by a mathematical statement, Eksample: 'two over five'.\nMathematical statement are: \n - plus \n - minus \n - times \n - over" << endl;
    cin >> first_string >> statement >> sencond_string;

    // Convert the string to floats
    first_float = string_to_float(first_string);
    second_float = string_to_float(sencond_string);

    // Calculate the result
    result = calculate(first_float,second_float,statement);

    // Display the result
    cout << "Result: " << result << endl;
}