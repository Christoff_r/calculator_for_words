# Calculator For Words

![banner](https://c.tenor.com/bxeM9N2IXLsAAAAd/osita-osita-iheme.gif)

![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen)
![language](https://img.shields.io/badge/language-c%2B%2B-blue)

## Table of Contents

- [Background](#background)
- [Install](#install)
- [Usage](#usage)
- [Contributing](#contributing)
- [License](#license)


## Background

The "Calculator For Words" takes a user input in form of a sentence. The sentence needs too comprise of a written number,  mathematical statement in words and a second written number, see [usage](#usage) for more details.

## Install
Below is a short description on how to install the program. It have no external dependencies or services.

Navigate to were you want the repository to be. Open the `CLI` and type the following:

```
git clone https://gitlab.com/Christoff_r/calculator_for_words.git
```

## Usage
In the repository open the `CLI` and type the following. 
```
cd build
./main
```
You will be promted to write a sentence with the following structure: "number [space] mathematical statement [space] number", eksampel:

"seven over two" or "six times nine".

When the sentence have been written press enter.
The result will be calculated and showen in the `CLI`.

### List of mathematical statement

- "plus" - eksample: "one plus eight"
- "minus" - eksample: "three minus two"
- "times" - eksample: "six times seven"
- "over" - eksample: "two plus five"

### List of numbers

- "zero"
- "one"
- "two"
- "three"
- "four"
- "five"
- "six"
- "seven"
- "eight"
- "nine"


## Contributing

If you have any tips, sugestions or things like that feel free to send me a message :speech_balloon:, although I might me slow to respond :sweat_smile:

## License

`UNLICENSED`)